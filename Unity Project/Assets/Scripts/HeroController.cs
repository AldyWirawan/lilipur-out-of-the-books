﻿using UnityEngine;
using System.Collections;

public class HeroController : MonoBehaviour {
	public float moveSpeed;
	private Vector3 moveDirection;
	private bool facingLeft;
	private int counter;
	// Use this for initialization
	void Start () {
		facingLeft = false;
		counter = 0;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 currentPosition = transform.position;
		// 2
		if( Input.GetKey(KeyCode.RightArrow) ){
		  if (facingLeft) {
			  transform.rotation = new Quaternion(transform.rotation.x, 0, transform.rotation.z, transform.rotation.w);
              facingLeft = false;
		  }
		  moveDirection = Vector3.right;
		  moveDirection.z = 0; 
		  moveDirection.Normalize();
		  Vector3 target = moveDirection * moveSpeed + currentPosition;
		  transform.position = Vector3.Lerp( currentPosition, target, Time.deltaTime );
		}
		
		if( Input.GetKey(KeyCode.LeftArrow)){
		  if (!facingLeft) {
			  transform.rotation = new Quaternion(transform.rotation.x, 180, transform.rotation.z, transform.rotation.w);
			  facingLeft = true;
		  }
		  moveDirection = Vector3.left;
		  moveDirection.z = 0;
		  moveDirection.Normalize();
		  Vector3 target = moveDirection * moveSpeed + currentPosition;
		  transform.position = Vector3.Lerp(currentPosition, target, Time.deltaTime);
		  
		}
	}
	void FixedUpdate(){
		if(Input.GetKey(KeyCode.UpArrow)&&counter == 0){
			rigidbody2D.AddForce(Vector3.up * 300);
			counter = 70;
		}
		if (counter > 0){
			counter -= 1;
		}
	}
}
