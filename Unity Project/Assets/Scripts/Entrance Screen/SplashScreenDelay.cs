﻿using UnityEngine;
using System.Collections;

public class SplashScreenDelay : MonoBehaviour {
    public float delayTime = 3;
    public int fadeOutTime = 1;
    public int fadeInTime = 1;
    public int LevelLoaded;

    IEnumerator Start()
    {
        yield return new WaitForSeconds(delayTime);

        AutoFade.LoadLevel(LevelLoaded,fadeOutTime,fadeInTime,Color.black);
    }
}
