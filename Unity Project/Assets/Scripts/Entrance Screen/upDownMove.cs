﻿using UnityEngine;
using System.Collections;

public class upDownMove : MonoBehaviour {

    bool up;
    bool down;

    Vector2 defaultPosition;
    Vector2 defaultDirection;
    float modifier;
    double animateCounter;

	// Use this for initialization
	void Start () {
        up = false;
        down = true;
        defaultPosition = transform.position;
        defaultDirection = new Vector2(defaultPosition.x, defaultPosition.y + (float)0.1);
        modifier = (float)0.02;
        animateCounter = 1;
	}
	
	// Update is called once per frame
    void Update()
    {
        if (up)
        {
            animateCounter -= Time.deltaTime;
            if (animateCounter >= 0)
            {
                transform.position = Vector2.Lerp(defaultPosition, defaultDirection, modifier);
                modifier += (float)0.02;
            }
            else
            {
                animateCounter = 1;
                up = false;
                down = true;
                modifier = (float)0.02;
            }
        }
        if (down)
        {
            animateCounter -= Time.deltaTime;
            if (animateCounter >= 0)
            {
                transform.position = Vector2.Lerp(defaultDirection, defaultPosition, modifier);
                modifier += (float)0.02;
            }
            else 
            {
                animateCounter = 1;
                up = true;
                down = false;
                modifier = (float)0.02;
            }

        }
    }
}
